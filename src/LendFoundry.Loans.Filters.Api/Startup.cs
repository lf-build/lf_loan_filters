﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Filters.Persistence;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Filters.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddLoanService(Settings.Loan.Host, Settings.Loan.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddTenantTime();

            // Interface resolvers
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IFilterViewRepository, FilterViewRepository>();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory>();
            services.AddTransient<ILoanChangesListener, LoanChangesListener>();
            services.AddTransient<ILoanFilterService, LoanFilterService>();
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
           
            // Aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();            
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseLoanChangesListener();
            app.UseMvc();
        }
    }
}
