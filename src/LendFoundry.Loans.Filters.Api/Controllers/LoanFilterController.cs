﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using RestSharp.Extensions.MonoHttp;
using System;
using System.Linq;

namespace LendFoundry.Loans.Filters.Api.Controllers
{
    [Route("/")]
    public class LoanFilterController : ExtendedController
    {
        public LoanFilterController(ILoanFilterService service) { Service = service; }

        private ILoanFilterService Service { get; }

        [HttpGet("in-grace-period/{paymentMethod}")]
        public IActionResult GetLoansInGracePeriod(PaymentMethod paymentMethod)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetLoansInGracePeriod(paymentMethod)));
        }

        [HttpGet("late/{minDaysDue}/to/{maxDaysDue}/days")]
        public IActionResult GetLoans(int minDaysDue, int maxDaysDue)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetLoans(minDaysDue, maxDaysDue)));
        }

        [HttpGet("late/{minDaysDue}/days")]
        public IActionResult GetLoans(int minDaysDue)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetLoans(minDaysDue)));
        }

        [HttpGet("status/{*statuses}")]
        public IActionResult GetLoans(string statuses)
        {
            return Execute(() =>
            {
                if (string.IsNullOrWhiteSpace(statuses))
                    throw new ArgumentException("At least one status must to be informed");

                // Solution to avoid problem when receiving encoded URL from Restshap.
                statuses = HttpUtility.UrlDecode(statuses);

                var loanStatuses = statuses.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(s => new LoanStatus(s, string.Empty));

                return new HttpOkObjectResult(Service.GetLoans(loanStatuses.ToArray()));
            });
        }

        [HttpGet("due-in/{dueDate}")]
        public IActionResult GetLoansDueIn(DateTime dueDate)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetLoansDueIn(dueDate)));
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            return Execute(() => new HttpOkObjectResult(Service.GetAll()));
        }
    }
}