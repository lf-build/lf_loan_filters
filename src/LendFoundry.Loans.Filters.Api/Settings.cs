﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Loans.Filters.Api
{
    public static class Settings
    { 
        private const string Prefix = "LOAN_FILTERS";

        public static string ServiceName { get; } = "loan-filters";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Loan { get; } = new ServiceSettings($"{Prefix}_LOAN", "loans");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "loans-filters");

    }
}
