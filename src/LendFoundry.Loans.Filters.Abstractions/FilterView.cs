﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        private double _amount;
        private double _rate;
        private double _amountDue;
        private double _remainingBalance;

        public string LoanReferenceNumber { get; set; }
        public string CustomerName { get; set; }

        public double Amount
        {
            get { return Math.Round(_amount, 2, MidpointRounding.AwayFromZero); }

            set { _amount = value; }
        }
        public double Rate
        {
            get { return Math.Round(_rate, 2, MidpointRounding.AwayFromZero); }

            set { _rate = value; }
        }
        public int Term { get; set; }
        public DateTimeOffset? LastPaymentDate { get; set; }
        public DateTimeOffset? DueDate { get; set; }
        public double AmountDue
        {
            get { return Math.Round(_amountDue, 2, MidpointRounding.AwayFromZero); }

            set { _amountDue = value; }
        }
        public int DaysPastDue { get; set; }
        public double RemainingBalance
        {
            get { return Math.Round(_remainingBalance, 2, MidpointRounding.AwayFromZero); }

            set { _remainingBalance = value; }
        }
        public string LoanStatusCode { get; set; }
        public bool IsInGracePeriod { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public string Investor { get; set; }
        public string NextRefreshSetStatusTo { get; set; }
    }
}