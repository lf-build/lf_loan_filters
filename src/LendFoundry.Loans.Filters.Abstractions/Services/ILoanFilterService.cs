﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Filters
{
    public interface ILoanFilterService
    {
        IEnumerable<IFilterView> GetLoansInGracePeriod(PaymentMethod paymentMethod);
        IEnumerable<IFilterView> GetLoans(int minDaysDue, int maxDaysDue);
        IEnumerable<IFilterView> GetLoans(int minDaysDue);
        IEnumerable<IFilterView> GetLoans(LoanStatus[] statuses);
        IEnumerable<IFilterView> GetLoansDueIn(DateTime dueDate);
        IEnumerable<IFilterView> GetAll();     
    }
}
