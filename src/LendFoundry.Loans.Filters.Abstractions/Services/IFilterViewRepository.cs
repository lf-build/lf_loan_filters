﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Loans.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        void AddOrUpdate(IFilterView view);
        string[] GetAllReferenceNumbers();
        IEnumerable<IFilterView> GetAll();
    }
}
