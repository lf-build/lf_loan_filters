using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}