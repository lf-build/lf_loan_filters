﻿namespace LendFoundry.Loans.Filters
{
    //todo: move all those hardcode values to configuration
    public class LoanStatusCodes
    {
        public static string OnBoarding => "100.01";
        public static string OnBoarded => "100.02";
        public static string InService => "100.03";
        public static string Delinquent => "100.04";
        public static string Collections => "100.05";
        public static string SkipTrace => "100.06";
        public static string Forbearance => "100.07";
        public static string CivilReliefAct => "100.08";
        public static string Bankruptcy => "100.09";
        public static string Legal => "200.01";
        public static string Research => "200.02";
        public static string Fraud => "200.03";
        public static string OtherIssues => "200.04";
        public static string Incarcerated => "200.05";
        public static string CeaseDesist => "200.06";
        public static string ChargedOff => "300.01";
        public static string Deceased => "300.02";
        public static string BankruptcyPaidOff => "300.03";
        public static string BankruptcyDischarged => "300.04";
        public static string PaidOff => "300.05";
        public static string ChargedOffFraud => "300.06";
    }
}
