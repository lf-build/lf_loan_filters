﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Filters
{
    public interface IFilterView : IAggregate
    {
        string LoanReferenceNumber { get; set; }
        string CustomerName { get; set; }
        double Amount { get; set; }
        double Rate { get; set; }
        int Term { get; set; }
        DateTimeOffset? LastPaymentDate { get; set; }
        DateTimeOffset? DueDate { get; set; }
        double AmountDue { get; set; }
        int DaysPastDue { get; set; }
        double RemainingBalance { get; set; }
        string LoanStatusCode { get; set; }
        bool IsInGracePeriod { get; set; }
        PaymentMethod PaymentMethod { get; set; }
        string Investor { get; set; }
        string NextRefreshSetStatusTo { get; set; }
    }
}