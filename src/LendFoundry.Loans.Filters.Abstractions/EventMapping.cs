﻿namespace LendFoundry.Loans.Filters
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string LoanReferenceNumber { get; set; }
    } 
}