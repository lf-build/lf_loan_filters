﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Loans.Filters.Client
{
    public class LoanFilterFactory : ILoanFilterFactory
    {
        public LoanFilterFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; set; }

        private string Endpoint { get; set; }

        private int Port { get; set; }

        public ILoanFilterService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new LoanFilterClient(client);
        }
    }
}
