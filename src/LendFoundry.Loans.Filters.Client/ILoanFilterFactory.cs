﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Filters.Client
{
    public interface ILoanFilterFactory
    {
        ILoanFilterService Create(ITokenReader reader);
    }
}
