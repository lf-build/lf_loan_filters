﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Filters.Client
{
    public static class LoanFilterExtensions
    {
        public static IServiceCollection AddLoanFilter(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ILoanFilterFactory>(p => new LoanFilterFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILoanFilterFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
