﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendFoundry.Loans.Filters.Client
{
    public class LoanFilterClient : ILoanFilterService
    {
        public LoanFilterClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("all", Method.GET);         
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetLoans(LoanStatus status)
        {
            return GetLoans(new[] { status });
        }

        public IEnumerable<IFilterView> GetLoans(LoanStatus[] statuses)
        {
            var request = new RestRequest("status/{statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses)
                strBuilder.Append($"{item.Code}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());            
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetLoans(int minDaysDue)
        {
            var request = new RestRequest("late/{minDaysDue}/days", Method.GET);
            request.AddUrlSegment("minDaysDue", minDaysDue.ToString());
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetLoans(int minDaysDue, int maxDaysDue)
        {
            var request = new RestRequest("late/{minDaysDue}/to/{maxDaysDue}/days", Method.GET);
            request.AddUrlSegment("minDaysDue", minDaysDue.ToString());
            request.AddUrlSegment("maxDaysDue", maxDaysDue.ToString());
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetLoansDueIn(DateTime dueDate)
        {
            var request = new RestRequest("due-in/{dueDate}", Method.GET);
            request.AddUrlSegment("dueDate", dueDate.ToString());
            return Client.Execute<IEnumerable<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetLoansInGracePeriod(PaymentMethod paymentMethod)
        {
            var request = new RestRequest("in-grace-period/{paymentMethod}", Method.GET);
            request.AddUrlSegment("paymentMethod", paymentMethod.ToString());
            return Client.Execute<IEnumerable<FilterView>>(request);
        }
    }
}
