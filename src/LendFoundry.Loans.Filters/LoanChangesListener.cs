﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Abstractions;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Schedule;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;

namespace LendFoundry.Loans.Filters
{
    public class LoanChangesListener : ILoanChangesListener
    {
        public LoanChangesListener
        (
            IConfigurationServiceFactory configurationServiceFactory,
            IConfigurationServiceFactory<Configuration> filtersConfigurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoanServiceFactory loanService,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory
        )
        {
            EventHubFactory = eventHubFactory;
            ConfigurationFactory = configurationServiceFactory;
            FiltersConfigurationFactory = filtersConfigurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            LoanService = loanService;
            TenantServiceFactory = tenantServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IConfigurationServiceFactory<Configuration> FiltersConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ILoanServiceFactory LoanService { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Loan changes eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var hub = EventHubFactory.Create(emptyReader);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    // Tenant token creation         
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation
                    var repository = RepositoryFactory.Create(reader);
                    var loanService = LoanService.Create(reader);
                    var investorsService = ConfigurationFactory.Create<InvestorService>("investors", reader).Get();
                    var loanFilterConfig = FiltersConfigurationFactory.Create(reader).Get();
                    var loanConfig = ConfigurationFactory.Create<LoanConfiguration>("loan", reader).Get();
                    var currentTenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);

                    // Attach all configured events to be listen
                    loanFilterConfig.Events
                                .ToList()
                                .ForEach(eventConfig =>
                    {
                        hub.On
                        (
                            eventName: eventConfig.Name,
                            handler: UpdateView(eventConfig, loanService, repository, investorsService, logger, loanConfig, currentTenantTime)
                        );
                    });

                });
                hub.StartAsync();
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub to process Loan changes", ex);
                Start();
            }
        }

        private static Action<EventInfo> UpdateView(
            EventMapping eventConfiguration, ILoanService loanService, IFilterViewRepository repository,
            InvestorService investorsService, ILogger logger, LoanConfiguration loanConfig, ITenantTime currentTenantTime)
        {
            if (eventConfiguration == null) throw new ArgumentNullException(nameof(eventConfiguration));
            if (loanService == null) throw new ArgumentNullException(nameof(loanService));
            if (repository == null) throw new ArgumentNullException(nameof(repository));
            if (investorsService == null) throw new ArgumentNullException(nameof(investorsService));
            if (logger == null) throw new ArgumentNullException(nameof(logger));

            return @event =>
            {
                try
                {
                    var loanReferenceNumber = eventConfiguration.LoanReferenceNumber.FormatWith(@event);
                    var loanDetails = loanService.GetLoanDetails(loanReferenceNumber);
                    if (loanDetails != null)

                    {
                        repository.AddOrUpdate(Parse(logger, @event, investorsService, loanDetails, loanConfig, currentTenantTime));
                        logger.Info($"Information for Loan #{loanReferenceNumber} from subscription #{@event.Name} has been processed");
                    }
                    else
                        logger.Warn($"Loan #{loanReferenceNumber} could not be found while processing subscription #{@event.Name}");
                }
                catch (Exception ex)
                {
                    logger.Error($"Failed to process event '{eventConfiguration.Name}'", ex);
                }
            };
        }

        private static FilterView Parse(
            ILogger logger, IEventInfo @event, InvestorService investorService, ILoanDetails loanDetails,
            LoanConfiguration loanConfig, ITenantTime currentTenantTime)
        {
            var loanInfo = loanDetails.LoanInfo;
            var payment = loanDetails.PaymentSchedule;

            //-----------------------
            logger.Info("");
            logger.Info("[START LOAN PARSE]");
            var borrower = loanInfo.Loan.Borrowers.FirstOrDefault(b => b.IsPrimary);
            logger.Info($"Primary Borrower Check...............DONE");
            //-----------------------
            var investor = investorService.FindById(loanInfo.Loan.Investor.Id);
            logger.Info($"Investor Check.......................DONE", investor);
            //-----------------------            
            var isDelinquent = loanInfo.Summary.IsDelinquent();
            var isDelinquentRollback = loanInfo.Summary.IsDelinquentRollback(loanInfo.Loan);
            logger.Info($"Delinquent Check.....................DONE");
            //-----------------------            
            var isPaidOff = payment.IsPaidOff(loanInfo.Loan.PaymentMethod, loanConfig, currentTenantTime);
            logger.Info($"PaidOff Check........................DONE ");
            //-----------------------

            var view = new FilterView
            {
                Investor = investor?.Name,
                LoanReferenceNumber = loanInfo.Loan.ReferenceNumber,
                LastPaymentDate = loanInfo.Summary.LastPaymentDate,
                DueDate = loanInfo.Summary.NextDueDate,
                Amount = loanInfo.Loan.Terms.LoanAmount,
                AmountDue = loanInfo.Summary.CurrentDue,
                DaysPastDue = loanInfo.Summary.DaysPastDue,
                Rate = loanInfo.Loan.Terms.Rate,
                RemainingBalance = loanInfo.Summary.RemainingBalance,
                Term = loanInfo.Loan.Terms.Term,
                PaymentMethod = loanInfo.Loan.PaymentMethod,
                IsInGracePeriod = loanInfo.Summary.IsInGracePeriod,
                TenantId = @event.TenantId,
                CustomerName = $"{borrower?.FirstName} {borrower?.LastName}",
                LoanStatusCode = loanInfo.Loan.Status.Code,
                NextRefreshSetStatusTo = ResolveStatus(isDelinquent, isPaidOff, isDelinquentRollback, loanInfo.Loan, loanConfig)
            };
            logger.Info("[END LOAN PARSE]");
            return view;
        }

        private static string ResolveStatus
        (
            bool isDelinquent,
            bool isPaidOff,
            bool isDelinquentRollback,
            ILoan loanInfo,
            LoanConfiguration loanConfig
        )
        {
            // Delinquent transition discovering
            if (isDelinquent && IsValidTransitionMove(loanConfig, loanInfo.Status.Code, LoanStatusCodes.Delinquent))
                return LoanStatusCodes.Delinquent;
            if (isDelinquentRollback && IsValidTransitionMove(loanConfig, loanInfo.Status.Code, LoanStatusCodes.InService))
                return LoanStatusCodes.InService;

            // Paidoff transition discovering        
            if (isPaidOff && IsValidTransitionMove(loanConfig, loanInfo.Status.Code, LoanStatusCodes.PaidOff))
                return LoanStatusCodes.PaidOff;
            return string.Empty;
        }

        private static bool IsValidTransitionMove(LoanConfiguration loanConfig, string actualStatus, string nextStatus)
        {
            // Get definition of current status.
            var statusConfig = loanConfig.LoanStatus.FirstOrDefault(s => s.Code == actualStatus);

            // Verify if next status is configured as a valid transition from current status.
            return (statusConfig.Transitions == null || !statusConfig.Transitions.Any(t => t == nextStatus)) == false;
        }
    }

    public static class DelinquentVerification
    {
        public static bool IsDelinquent(this IPaymentScheduleSummary summary) =>
            summary.CurrentDue > 0 && !summary.IsInGracePeriod;

        public static bool IsDelinquentRollback(this IPaymentScheduleSummary summary, ILoan loan)
        {
            return IsDelinquent(summary) == false &&
                   loan.Status.Code == LoanStatusCodes.Delinquent &&
                   loan.Status.Code != LoanStatusCodes.PaidOff;
        }
    }

    public static class PaidOffVerification
    {
        public static bool IsPaidOff(this IPaymentSchedule payment, PaymentMethod method, LoanConfiguration loanConfig, ITenantTime currentTenantTime)
        {
            if (payment.LastPaidInstallment == null)
                return false;

            var coolingPeriod = method == PaymentMethod.ACH ? loanConfig.Schedule.AchCoolingPeriod :
                                                              loanConfig.Schedule.CheckCoolingPeriod;

            return payment.LastPaidInstallment.EndingBalance.Equals(0.0) &&
                   IsCoolingPeriodExpired(payment, coolingPeriod, currentTenantTime) &&
                   AllInstallmentsAreCompleted(payment);
        }

        private static bool IsCoolingPeriodExpired(IPaymentSchedule payment, int coolingPeriod, ITenantTime currentTenantTime)
        {
            var businessDays = payment.LastPaidInstallment.DueDate?.BusinessDaysTo(currentTenantTime.Now);
            return businessDays >= coolingPeriod;
        }

        private static bool AllInstallmentsAreCompleted(IPaymentSchedule payment)
        {
            return payment.Installments.All(a => a.Status == InstallmentStatus.Completed);
        }
    }
}
