﻿using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Filters
{
    public static class LoanChangesListenerExtensions
    {
        public static void UseLoanChangesListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<ILoanChangesListener>().Start();
        }
    }
}
