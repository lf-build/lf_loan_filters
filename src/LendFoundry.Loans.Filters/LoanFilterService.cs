﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Loans.Filters
{
    public class LoanFilterService : ILoanFilterService
    {
        public LoanFilterService
        (
            IFilterViewRepository repository,
            ITenantTime tenantTime,
            IEventHubClient eventHub,
            ILogger logger
        )
        {
            Repository = repository;
            TenantTime = tenantTime;
            EventHub = eventHub;
            Logger = logger;
        }

        private IFilterViewRepository Repository { get; }

        private ITenantTime TenantTime { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        public IEnumerable<IFilterView> GetLoansInGracePeriod(PaymentMethod paymentMethod)
        {
            return Repository
                .All(q => q.IsInGracePeriod &&
                          q.PaymentMethod == paymentMethod &&
                          q.LoanStatusCode == LoanStatusCodes.InService)
                .Result;
        }

        public IEnumerable<IFilterView> GetLoans(int minDaysDue, int maxDaysDue)
        {
            if (minDaysDue < 0)
                throw new ArgumentException($"{nameof(minDaysDue)} cannot be negative");

            if (maxDaysDue < 0)
                throw new ArgumentException($"{nameof(maxDaysDue)} cannot be negative");

            if (minDaysDue > maxDaysDue)
                throw new ArgumentException($"{nameof(minDaysDue)} cannot be greater than {nameof(maxDaysDue)}");

            Expression<Func<IFilterView, bool>> query =
                q => q.DaysPastDue >= minDaysDue &&
                     q.DaysPastDue <= maxDaysDue &&
                     q.LoanStatusCode == LoanStatusCodes.Delinquent;

            return Repository.All(query).Result;
        }

        public IEnumerable<IFilterView> GetLoans(int minDaysDue)
        {
            if (minDaysDue < 0)
                throw new ArgumentException($"{nameof(minDaysDue)} cannot be negative");

            return Repository
                .All(q => q.DaysPastDue >= minDaysDue &&
                          q.LoanStatusCode == LoanStatusCodes.Delinquent)
                .Result;
        }

        public IEnumerable<IFilterView> GetLoans(LoanStatus[] statuses)
        {
            if (statuses == null)
                throw new ArgumentException($"{nameof(statuses)} cannot be null");

            if (statuses.Count() == 1)
            {
                var status = statuses.First();

                if (string.IsNullOrWhiteSpace(status.Code))
                    throw new ArgumentException($"{nameof(status)}.Code cannot be null");

                return Repository
                    .All(q => q.LoanStatusCode == status.Code)
                    .Result;
            }
            else
            {
                var codes = statuses.Select(s => s.Code);
                return Repository
                    .All(q => codes.Contains(q.LoanStatusCode))
                    .Result;
            }
        }

        public IEnumerable<IFilterView> GetLoansDueIn(DateTime dueDate)
        {
            var date = TenantTime.FromDate(dueDate);

            return Repository
                .All(q => q.DueDate == date)
                .Result;
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return Repository.GetAll();
        }
    }
}
