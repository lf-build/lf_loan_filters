using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Filters
{
    public class InvestorService
    {
        public List<Investor> Investors { get; set; }

        public Investor FindById(string id)
        {
            return Investors.FirstOrDefault(i => i.Id == id);
        }
    }
}