﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Loans.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.LastPaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(p => p.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(f => f.PaymentMethod).SetSerializer(new EnumSerializer<PaymentMethod>(BsonType.String));

                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "loan-filters")
        {
            CreateIndexIfNotExists("due-date", Builders<IFilterView>.IndexKeys.Ascending(i => i.DueDate));
            CreateIndexIfNotExists("days-past-due", Builders<IFilterView>.IndexKeys.Ascending(i => i.DaysPastDue));
            CreateIndexIfNotExists("status-code", Builders<IFilterView>.IndexKeys.Ascending(i => i.LoanStatusCode));
            CreateIndexIfNotExists("payment-method", Builders<IFilterView>.IndexKeys.Ascending(i => i.PaymentMethod));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.LoanReferenceNumber == view.LoanReferenceNumber;

            var existingId = Query
                .Where(v => v.LoanReferenceNumber == view.LoanReferenceNumber)
                .Select(v => v.Id)
                .FirstOrDefault();

            view.TenantId = TenantService.Current.Id;
            view.Id = existingId ?? ObjectId.GenerateNewId().ToString();

            Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
        }

        public string[] GetAllReferenceNumbers()
        {
            return Query.Select(q => q.LoanReferenceNumber).ToArray();
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return All(x => x.TenantId == TenantService.Current.Id).Result;
        }
    }
}
