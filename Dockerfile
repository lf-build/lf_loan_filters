FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Loans.Filters.Abstractions /app/LendFoundry.Loans.Filters.Abstractions
WORKDIR /app/LendFoundry.Loans.Filters.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Filters.Persistence /app/LendFoundry.Loans.Filters.Persistence
WORKDIR /app/LendFoundry.Loans.Filters.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Filters /app/LendFoundry.Loans.Filters
WORKDIR /app/LendFoundry.Loans.Filters
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Filters.Client /app/LendFoundry.Loans.Filters.Client
WORKDIR /app/LendFoundry.Loans.Filters.Client
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Filters.Api /app/LendFoundry.Loans.Filters.Api
WORKDIR /app/LendFoundry.Loans.Filters.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel