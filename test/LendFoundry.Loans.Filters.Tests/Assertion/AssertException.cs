﻿using System;
using Xunit.Sdk;

namespace LendFoundry.Loans.Filters.Tests.Assertion
{
    public static class AssertException
    {
        public static void Throws<T>(string expectedMessage, Action action) where T : Exception
        {
            bool hasExpectedExceptionBeenThrown = false;

            try
            {
                action();
            }
            catch (Exception exception)
            {
                if (exception is T)
                {
                    if (exception.Message == expectedMessage)
                    {
                        hasExpectedExceptionBeenThrown = true;
                    }
                    else
                    {
                        throw new AssertActualExpectedException(expectedMessage, exception.Message, "Wrong exception message");
                    }
                }
                else
                {
                    throw new AssertActualExpectedException(
                        typeof(T),
                        exception.GetType(),
                        "Unexpected exception thrown: " + exception.Message
                    );
                }
            }

            if (!hasExpectedExceptionBeenThrown)
            {
                throw new XunitException("Expected exception never thrown: " + typeof(T));
            }
        }
    }
}
