﻿using Microsoft.AspNet.Mvc;

namespace LendFoundry.Loans.Filters.Tests.Assertion
{
    public static class ActionResultExtensions
    {
        public static ObjectResult Result(this IActionResult action)
        {
            return (ObjectResult)action;
        }
    }
}
