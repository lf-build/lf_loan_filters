﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace LendFoundry.Loans.Filters.Tests.Fakes
{
    public class FakeServiceClient<T> : IServiceClient
    {
        public IEnumerable<T> InMemoryData { get; set; }

        public bool Execute(IRestRequest request)
        {
            return true;
        }

        public object Execute(IRestRequest request, Type type)
        {
            throw new NotImplementedException();
        }

        public T Execute<T>(IRestRequest request)
        {            
            if (InMemoryData != null)
                return (T)(IEnumerable<FilterView>)InMemoryData;
            return default(T);
        }

        public Task<bool> ExecuteAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteAsync(IRestRequest request, Type type)
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteAsync<T>(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public IRestResponse ExecuteRequest(IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<IRestResponse> ExecuteRequestAsync(IRestRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
