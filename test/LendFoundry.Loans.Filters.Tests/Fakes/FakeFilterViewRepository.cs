﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Filters.Tests.Fakes
{
    public class FakeFilterViewRepository : IFilterViewRepository
    {
        private static List<IFilterView> InMemoryData { get; set; } = new List<IFilterView>();

        public void Add(IFilterView item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (string.IsNullOrWhiteSpace(view.Id))
                Add(view);
            else
                Update(view);
        }

        public Task<IEnumerable<IFilterView>> All(Expression<Func<IFilterView, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InMemoryData.AsQueryable().Where(query).AsEnumerable();
            });
        }

        public int Count(Expression<Func<IFilterView, bool>> query)
        {
            return InMemoryData.Count();
        }

        public Task<IFilterView> Get(string id)
        {
            return Task.Run(() =>
            {
                return InMemoryData.AsQueryable().FirstOrDefault(x => x.Id == id);
            });
        }

        public IEnumerable<IFilterView> GetAll()
        {
            throw new NotImplementedException();
        }

        public string[] GetAllReferenceNumbers()
        {
            return InMemoryData.Select(a => a.LoanReferenceNumber).ToArray();
        }

        public void Remove(IFilterView item)
        {
            InMemoryData = InMemoryData.Where(x => x.Id != item.Id).ToList();
        }

        public void Update(IFilterView item)
        {
            InMemoryData = InMemoryData.Select(x =>
            {
                if (x.Id == item.Id)
                    x = item;
                return x;
            }).ToList();
        }
    }
}
