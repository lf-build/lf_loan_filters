﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Filters.Tests.Assertion;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Filters.Tests
{
    public class LoanFiltersServiceTest
    {
        private Mock<ITenantTime> TenantTime = new Mock<ITenantTime>();

        private Mock<IEventHubClient> EventHubClient = new Mock<IEventHubClient>();

        private Mock<ILogger> Logger = new Mock<ILogger>();

        [Fact]
        public void GetLoansByStatus_SingleCode_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.01" });
            repository.Add(new FilterView { LoanStatusCode = "100.01" });
            repository.Add(new FilterView { LoanStatusCode = "100.02" });
            repository.Add(new FilterView { LoanStatusCode = "100.03" });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(new[] { new LoanStatus("100.01", "") });

            Assert.True(result.All(a => a.LoanStatusCode == "100.01"));
        }

        [Fact]
        public void GetLoansByStatus_SingleCode_HasNoData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.03" });
            repository.Add(new FilterView { LoanStatusCode = "100.03" });
            repository.Add(new FilterView { LoanStatusCode = "100.05" });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(new[] { new LoanStatus("100.09", "") });

            Assert.Empty(result);
        }

        [Fact]
        public void GetLoansByStatus_SingleCode_HasInvalidStatusCode()
        {
            var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
            Assert.Throws<ArgumentException>(() =>
            {
                service.GetLoans(null);
            });

            Assert.Throws<ArgumentException>(() =>
            {
                service.GetLoans(new[] { new LoanStatus(null, "") });
            });
        }

        [Fact]
        public void GetLoansByStatus_MultiCode_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.01" });
            repository.Add(new FilterView { LoanStatusCode = "100.02" });
            repository.Add(new FilterView { LoanStatusCode = "200.05" });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(new[] {
                new LoanStatus("100.01", ""),
                new LoanStatus("100.02", ""),
                new LoanStatus("300.01", "")
            });

            Assert.True(result.Any());
            Assert.True(result.Any(a => a.LoanStatusCode == "100.01"));
            Assert.True(result.Any(a => a.LoanStatusCode == "100.02"));
            Assert.False(result.Any(a => a.LoanStatusCode == "300.01"));
        }

        [Fact]
        public void GetLoansByStatus_MultiCode_HasNoData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.01" });
            repository.Add(new FilterView { LoanStatusCode = "100.02" });
            repository.Add(new FilterView { LoanStatusCode = "100.03" });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(new[] {
                new LoanStatus("200.01", ""),
                new LoanStatus("200.02", ""),
                new LoanStatus("200.04", "")
            });

            Assert.Empty(result);
        }

        [Fact]
        public void GetLoansInGracePeriod_ACH_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.03", IsInGracePeriod = true, PaymentMethod = PaymentMethod.ACH });
            repository.Add(new FilterView { LoanStatusCode = "100.03", IsInGracePeriod = false, PaymentMethod = PaymentMethod.Check });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoansInGracePeriod(PaymentMethod.ACH);

            Assert.NotEmpty(result);
            Assert.Equal(1, result.Count());
            Assert.True(result.All(a => a.PaymentMethod == PaymentMethod.ACH));
            Assert.True(result.All(a => a.LoanStatusCode == "100.03"));
            Assert.True(result.All(a => a.IsInGracePeriod == true));
        }

        [Fact]
        public void GetLoansInGracePeriod_Check_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.03", IsInGracePeriod = false, PaymentMethod = PaymentMethod.ACH });
            repository.Add(new FilterView { LoanStatusCode = "100.03", IsInGracePeriod = true, PaymentMethod = PaymentMethod.Check });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoansInGracePeriod(PaymentMethod.Check);

            Assert.NotEmpty(result);
            Assert.Equal(1, result.Count());
            Assert.True(result.All(a => a.PaymentMethod == PaymentMethod.Check));
            Assert.True(result.All(a => a.LoanStatusCode == "100.03"));
            Assert.True(result.All(a => a.IsInGracePeriod == true));
        }

        [Fact]
        public void GetLoansByMinMaxDays_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.03", DaysPastDue = 5 });
            repository.Add(new FilterView { LoanStatusCode = "100.04", DaysPastDue = 8 });
            repository.Add(new FilterView { LoanStatusCode = "100.04", DaysPastDue = 9 });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(5, 10);

            Assert.True(result.Any());
            Assert.True(result.All(x => x.LoanStatusCode == "100.04"));
            Assert.True(result.All(x => x.DaysPastDue >= 5));
            Assert.True(result.All(x => x.DaysPastDue <= 10));
        }

        [Fact]
        public void GetLoans_HasMinDayButNotMaxDays()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
                service.GetLoans(5, int.MinValue);
            });
        }

        [Fact]
        public void GetLoans_HasMaxDayButNotMinDays()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
                service.GetLoans(int.MinValue, 10);
            });
        }

        [Fact]
        public void GetLoans_HasMinDaysLessThenZero()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
                service.GetLoans(-1, 10);
            });
        }

        [Fact]
        public void GetLoans_HasMinDaysGreaterThenMaxDays()
        {
            AssertException.Throws<ArgumentException>("minDaysDue cannot be greater than maxDaysDue", () =>
            {
                var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
                service.GetLoans(5, 2);
            });
        }

        [Fact]
        public void GetLoansByMinDays_HasData()
        {
            var repository = new Fakes.FakeFilterViewRepository();
            repository.Add(new FilterView { LoanStatusCode = "100.03", DaysPastDue = 5 });
            repository.Add(new FilterView { LoanStatusCode = "100.04", DaysPastDue = 2 });
            repository.Add(new FilterView { LoanStatusCode = "100.04", DaysPastDue = 9 });
            var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            var result = service.GetLoans(5);

            Assert.True(result.Any());
            Assert.Equal(1, result.Count());
            Assert.True(result.All(x => x.DaysPastDue >= 5));
            Assert.True(result.All(x => x.LoanStatusCode == "100.04"));
        }

        [Fact]
        public void GetLoansByMinDays_HasMinDaysLessThenZero()
        {
            AssertException.Throws<ArgumentException>("minDaysDue cannot be negative", () =>
            {
                var service = new LoanFilterService(new Fakes.FakeFilterViewRepository(), TenantTime.Object, EventHubClient.Object, Logger.Object);
                service.GetLoans(-1);
            });
        }

        [Fact]
        public void GetLoansDelinquent_HasData()
        {
            //var repository = new Fakes.FakeFilterViewRepository();
            //repository.Add(new FilterView { NextRefreshSetStatusTo = string.Empty });
            //repository.Add(new FilterView { NextRefreshSetStatusTo = "100.04" });
            //repository.Add(new FilterView { NextRefreshSetStatusTo = "100.04" });
            //var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            //var result = service.GetLoansDelinquent();

            //Assert.True(result.Any());
            //Assert.Equal(2, result.Count());
            //Assert.True(result.All(x => x.IsDelinquent));
        }

        [Fact]
        public void GetLoansDelinquent_HasNoData()
        {
            //var repository = new Fakes.FakeFilterViewRepository();
            //repository.Add(new FilterView { IsDelinquent = false });
            //repository.Add(new FilterView { IsDelinquent = false });
            //repository.Add(new FilterView { IsDelinquent = false });
            //var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            //var result = service.GetLoansDelinquent();

            //Assert.True(!result.Any());
            //Assert.Equal(0, result.Count());
        }

        [Fact]
        public void GetLoansPaidOff_HasData()
        {
            //var repository = new Fakes.FakeFilterViewRepository();
            //repository.Add(new FilterView { IsPaidOff = false });
            //repository.Add(new FilterView { IsPaidOff = true });
            //repository.Add(new FilterView { IsPaidOff = true });
            //var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            //var result = service.GetLoansPaidOff();

            //Assert.True(result.Any());
            //Assert.Equal(2, result.Count());
            //Assert.True(result.All(x => x.IsPaidOff));
        }

        [Fact]
        public void GetLoansPaidOff_HasNoData()
        {
            //var repository = new Fakes.FakeFilterViewRepository();
            //repository.Add(new FilterView { IsPaidOff = false });
            //repository.Add(new FilterView { IsPaidOff = false });
            //repository.Add(new FilterView { IsPaidOff = false });
            //var service = new LoanFilterService(repository, TenantTime.Object, EventHubClient.Object, Logger.Object);

            //var result = service.GetLoansPaidOff();

            //Assert.True(!result.Any());
            //Assert.Equal(0, result.Count());
        }
    }
}
