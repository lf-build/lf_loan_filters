﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Filters.Client;
using LendFoundry.Security.Tokens;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Filters.Tests
{
    public class LoanFilterClientTests
    {
        private readonly Mock<IServiceClient> ServiceClient = new Mock<IServiceClient>();

        private LoanFilterClient CreateClient { get { return new LoanFilterClient(ServiceClient.Object); } }

        private void StubServiceClient()
        {
            ServiceClient.Setup(s => s.Execute<IEnumerable<IFilterView>>(It.IsAny<IRestRequest>())).Returns(It.IsAny<IEnumerable<IFilterView>>);
        }

        [Fact]
        public void GetLoansByStatusWhenSingleOne()
        {
            var serviceClient = new Fakes.FakeServiceClient<FilterView>();
            serviceClient.InMemoryData = new[] {
                new FilterView { LoanStatusCode = "100.01" },
                new FilterView { LoanStatusCode = "100.01" }                
            };
            var client = new LoanFilterClient(serviceClient);

            var result = client.GetLoans(new LoanStatus("100.01", ""));

            Assert.NotEmpty(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void GetLoansByStatusWhenManyCodes()
        {
            var serviceClient = new Fakes.FakeServiceClient<FilterView>();
            serviceClient.InMemoryData = new[] {
                new FilterView { LoanStatusCode = "100.01" },
                new FilterView { LoanStatusCode = "100.02" },
                new FilterView { LoanStatusCode = "100.03" }
            };
            var client = new LoanFilterClient(serviceClient);

            var result = client.GetLoans(new[] {"100.01", "100.02", "100.03" }
                               .Select(x => new LoanStatus(x, "")).ToArray());

            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void GetLoansByMinDaysDue()
        {
            var serviceClient = new Fakes.FakeServiceClient<FilterView>();
            serviceClient.InMemoryData = new[] {
                new FilterView {},
                new FilterView {},
                new FilterView {}
            };
            var client = new LoanFilterClient(serviceClient);

            var result = client.GetLoans(int.MinValue);

            Assert.NotEmpty(result);
            Assert.Equal(3, result.Count());
        }

        [Fact(Skip = "Refector it")]
        public void GetLoansByMinDaysDueAndMaxDaysDue()
        {
            StubServiceClient();
            var client = CreateClient;
            var result = client.GetLoans(int.MinValue, int.MaxValue);
            ServiceClient.Verify(v => v.Execute<IEnumerable<IFilterView>>(It.IsAny<IRestRequest>()));
        }

        [Fact(Skip = "Refector it")]
        public void GetLoansByDueDate()
        {
            StubServiceClient();
            var client = CreateClient;
            var result = client.GetLoansDueIn(new DateTime(2015, 12, 12));
            ServiceClient.Verify(v => v.Execute<IEnumerable<IFilterView>>(It.IsAny<IRestRequest>()));
        }

        [Fact(Skip = "Refector it")]
        public void GetLoansInGracePeriod()
        {
            StubServiceClient();
            var client = CreateClient;
            var result = client.GetLoansInGracePeriod(PaymentMethod.ACH);
            ServiceClient.Verify(v => v.Execute<IEnumerable<IFilterView>>(It.IsAny<IRestRequest>()));
        }

        [Fact(Skip = "Refector it")]
        public void GetLoans_InfinityRoute()
        {
            // Eventhub host
            string host = "10.1.1.99";
            int port = 5008;

            // Token generation
            var tokenHandler = new TokenHandler(null, null, null);
            var token = tokenHandler.Issue("my-tenant", "some-app");
            var reader = new StaticTokenReader(token.Value);

            // Service client generation
            IServiceClient serviceClient = new ServiceClient(
                accessor: new EmptyHttpContextAccessor(),
                logger: Mock.Of<ILogger>(),
                tokenReader: reader,
                endpoint: host,
                port: port
            );

            var client = new LoanFilterClient(serviceClient);

            var result = client.GetLoans(new[] {
                 new LoanStatus("100.01", ""),
                 new LoanStatus("100.02", ""),
                 new LoanStatus("100.03", ""),
            });

            var result2 = client.GetLoans(new LoanStatus("100.02", ""));
            if (result2 != null)
            {

            }
        }
    }
}
