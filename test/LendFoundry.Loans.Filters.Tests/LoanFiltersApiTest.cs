﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Filters.Api.Controllers;
using LendFoundry.Loans.Filters.Tests.Assertion;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;

namespace LendFoundry.Loans.Filters.Tests
{
    public class LoanFiltersApiTest
    {
        private readonly Mock<ILoanFilterService> Service;

        public LoanFilterController CreateController
        {
            get
            {
                return new LoanFilterController(Service.Object);
            }
        }

        public LoanFiltersApiTest()
        {
            Service = new Mock<ILoanFilterService>();
        }

        [Fact]
        public void GetLoansInGracePeriod_HasPaymentMethodThenReturnLoanList()
        {
            var paymentMethod = It.IsAny<PaymentMethod>();
            Service.Setup(s => s.GetLoansInGracePeriod(paymentMethod))
                    .Returns(new List<IFilterView>());

            var controller = CreateController;
            var result = controller.GetLoansInGracePeriod(paymentMethod).Result();

            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value);
            Assert.IsType<List<IFilterView>>(result.Value);
            Service.Verify(v => v.GetLoansInGracePeriod(It.IsAny<PaymentMethod>()));
        }

        [Fact]
        public void GetLoansByMinMaxDays_HasValidMinMaxDaysDueThenReturnLoanList()
        {
            var mindays = It.IsAny<int>();
            var maxdays = It.IsAny<int>();
            Service.Setup(s => s.GetLoans(mindays, maxdays))
                    .Returns(new List<IFilterView>());

            var controller = CreateController;
            var result = controller.GetLoans(DateTime.Now.Day,
                                             DateTime.Now.AddDays(2).Day).Result();

            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value);
            Assert.IsType<List<IFilterView>>(result.Value);
            Service.Verify(v => v.GetLoans(It.IsAny<int>(), It.IsAny<int>()));
        }

        [Fact]
        public void GetLoansByMinDays_HasValidMinDaysDueThenReturnLoanList()
        {
            Service.Setup(s => s.GetLoans(It.IsAny<int>()))
                    .Returns(new List<IFilterView>());

            var controller = CreateController;
            var result = controller.GetLoans(5).Result();

            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value);
            Assert.IsType<List<IFilterView>>(result.Value);
            Service.Verify(v => v.GetLoans(It.IsAny<int>()));
        }

        [Fact]
        public void GetLoansDueIn_HasValidDueDateThenReturnLoanList()
        {
            Service.Setup(s => s.GetLoansDueIn(It.IsAny<DateTime>()))
                    .Returns(new List<IFilterView>());

            var controller = CreateController;
            var result = controller.GetLoansDueIn(DateTime.UtcNow).Result();

            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value);
            Assert.IsType<List<IFilterView>>(result.Value);
            Service.Verify(v => v.GetLoansDueIn(It.IsAny<DateTime>()));
        }

        [Fact]
        public void GetLoansByStatus_HasValidLoanStatusThenReturnLoanList()
        {
            Service.Setup(s => s.GetLoans(It.IsAny<LoanStatus[]>()))
                    .Returns(new List<IFilterView>());

            var controller = CreateController;
            var result = controller.GetLoans("100.01").Result();

            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value);
            Assert.IsType<List<IFilterView>>(result.Value);
            Service.Verify(v => v.GetLoans(It.IsAny<LoanStatus[]>()));
        }

        [Fact]
        public void GetLoans_WhenUnhandledExceptionReturnInternalServerError()
        {
            Assert.Throws<Exception>(() =>
            {
                Service.Setup(s => s.GetLoans(It.IsAny<LoanStatus[]>()))
                        .Throws(new Exception());

                var controller = CreateController;
                var result = controller.GetLoans("100.01").Result();

                Assert.Equal(500, result.StatusCode);
                Assert.NotNull(result.Value);
                Assert.IsType<Error>(result.Value);
                Service.Verify(v => v.GetLoans(It.IsAny<LoanStatus[]>()));
            });
        }

        [Fact]
        public void GetLoans_LoanNotFoundThenReturnNotFoundObject()
        {            
            Service.Setup(s => s.GetLoans(It.IsAny<LoanStatus[]>()))
                    .Throws(new NotFoundException(string.Empty));

            var controller = CreateController;
            var result = (ErrorResult)controller.GetLoans("100.01");

            Assert.Equal(404, result.StatusCode);
            Service.Verify(v => v.GetLoans(It.IsAny<LoanStatus[]>()));
        }

        [Fact]
        public void Controller_HasBadFormatParamThenReturnBadRequest()
        {            
            Service.Setup(s => s.GetLoans(It.IsAny<LoanStatus[]>()))
                    .Throws(new ArgumentException(string.Empty));

            var controller = CreateController;
            var result = (ErrorResult)controller.GetLoans("100.01");

            Assert.Equal(400, result.StatusCode);
            Service.Verify(v => v.GetLoans(It.IsAny<LoanStatus[]>()));
        }


        [Fact]
        public void GetLoans_MultiCodeRouteParse()
        {
            Service.Setup(s => s.GetLoans(It.IsAny<LoanStatus[]>())).Returns(It.IsAny<IEnumerable<IFilterView>>());                    
            var controller = CreateController;
            var result = (HttpOkObjectResult)controller.GetLoans("100.01/100.02/100.03/100.04");

            Assert.Equal(200, result.StatusCode);            
        }
    }
}