﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Filters.Persistence;
using LendFoundry.Tenant.Client;
using Moq;
using System;


namespace LendFoundry.Loans.Filters.Tests
{
    public class FilterViewRepositoryTest
    {
        private string TenantId { get; } = "my-tenant";

        private Mock<ITenantService> TenantService { get; set; }

        private IFilterView FilterViewMock
        {
            get
            {
                return new FilterView
                {
                    Amount = 10,
                    AmountDue = 10,
                    CustomerName = "CustomerName",
                    DaysPastDue = 10,
                    DueDate = DateTimeOffset.UtcNow,
                    IsInGracePeriod = true,
                    LastPaymentDate = DateTimeOffset.UtcNow,
                    LoanReferenceNumber = string.Empty,
                    LoanStatusCode = "LoanStatusCode",
                    PaymentMethod = PaymentMethod.ACH,
                    Rate = 10,
                    RemainingBalance = 10,
                    Term = 10
                };
            }
        }

        private IFilterViewRepository Repository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new FilterViewRepository
            (
                configuration: config,
                tenantService: TenantService.Object
            );
        }

        //[MongoFact]
        //public void Add()
        //{
        //    MongoTest.Run((config) =>
        //    {
        //        // arrange
        //        string loanReferenceNumber = "loan01";
        //        var repo = Repository(config);
        //        var view = FilterViewMock;
        //        view.LoanReferenceNumber = loanReferenceNumber;

        //        // act
        //        repo.AddOrUpdate(view);

        //        // assert
        //        var loan = repo.Get(view.Id).Result;
        //        Assert.NotNull(loan);
        //        Assert.Equal(loan.TenantId, TenantId);
        //        Assert.False(string.IsNullOrWhiteSpace(loan.Id));
        //        Assert.Equal(loan.LoanReferenceNumber, loanReferenceNumber);
        //    });
        //}

        //[MongoFact]
        //public void Update()
        //{
        //    MongoTest.Run((config) =>
        //    {
        //        // arrange
        //        string loanReferenceNumber = "loan01";
        //        var repo = Repository(config);
        //        var view = FilterViewMock;
        //        view.LoanReferenceNumber = loanReferenceNumber;
        //        repo.AddOrUpdate(view);
        //        view.CustomerName = "updated";
        //        view.IsInGracePeriod = false;
        //        var generatedId = view.Id;

        //        // act
        //        repo.AddOrUpdate(view);

        //        // assert
        //        var loan = repo.Get(view.Id).Result;
        //        Assert.NotNull(loan);
        //        Assert.Equal(loan.TenantId, TenantId);
        //        Assert.False(string.IsNullOrWhiteSpace(loan.Id));
        //        Assert.Equal(loan.LoanReferenceNumber, loanReferenceNumber);
        //        Assert.Equal(loan.CustomerName, "updated");
        //        Assert.Equal(loan.IsInGracePeriod, false);
        //        Assert.Equal(loan.Id, generatedId);
        //    });
        //}

        //[MongoFact]
        //public void GetAllReferenceNumbers()
        //{
        //    MongoTest.Run((config) =>
        //    {
        //        // arrange                
        //        var repo = Repository(config);
        //        new[] { "loan01", "loan02", "loan03" }
        //            .ToList().ForEach(x =>
        //            {
        //                var view = FilterViewMock;
        //                view.LoanReferenceNumber = x;
        //                repo.AddOrUpdate(view);
        //            });

        //        // act
        //        var loans = repo.GetAllReferenceNumbers();

        //        // assert
        //        Assert.NotNull(loans);
        //        Assert.Equal(3, loans.Count());
        //    });
        //}
    }
}
